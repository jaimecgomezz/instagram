const graphQLHTTP = require("express-graphql");
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const bodyParser = require("body-parser");

import User from './src/models/users'
import schema from './src/graphql'

import {reciveToken} from './src/envVar'
import {createToken} from './src/resolvers/create'
import {verifyToken} from './src/resolvers/verify'
import {queryUsersType} from './src/graphql/querys/users';
import {graphql} from 'graphql'

const PORT = process.env.PORT || 3000;

mongoose.connect("mongodb://JaimeCGomez:1a2b3c@ds125831.mlab.com:25831/instagram-cn", {useNewUrlParser:true});

// mongoose.connect("mongodb://localhost:27017/local", {useNewUrlParser:true});

const db = mongoose.connection;
db.on('error', ()=>{
    console.log("No fue posible conectar con la base de datos madafaka");
}).once('open', ()=>{
    console.log("Se pudo conectar con la base de datos chavo");
})

app.use(bodyParser.json());

// app.use('/graphql', (req, res, next)=>{
//     const token = req.headers['authorization'];
//     try{
//         req.user = verifyToken(token);
//         next();
//     }catch(error){
//         res.status(401).json({message: error.message})
//     }
// })

app.use('/graphql',graphQLHTTP((req,res) =>({
    schema,
    graphiql:true,
    pretty:true,
    context:{
        user: req.user
    }
})));


app.get('/', (req, res)=>{
    // res.send({"message":"holi"})
    res.send("Estoy jalandoooo")
});



app.post('/signUp', (req,res)=>{
    let user = req.body;
    User.create(user).then((user)=>{
        res.status(201).json({
            "Fel":"Felicidades, ha logrado crear su cuenta",
            "id":user._id
        });
    }).catch((error)=>{
        res.status(400).json({message: error.message})
    })
});

app.post('/login', (req, res, next)=>{
    let user = req.body;
    createToken(user.email, user.password).then((token)=>{
        res.status(201).json({token});
    }).catch((err)=>{
        res.status(403).json({"Mensaje":"Hubo un problema al verificar credenciales"})
    })
})

app.post('/query', (req, res, next)=>{
    let lol = req.body;
    graphql(queryUsersType, query).then((resu)=>{
        console.log(resu);
        res.send(resu);
    })
})

app.listen(PORT, ()=>{
    console.log("Estoy funcionando en el puerto 3000");
});