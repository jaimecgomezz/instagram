'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _graphql = require('graphql');

var _users = require('../../../models/users');

var _users2 = _interopRequireDefault(_users);

var _users3 = require('../../types/users');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var addUser = {
    type: _users3.listUsersType,
    args: {
        data: {
            name: "data",
            type: new _graphql.GraphQLNonNull(_users3.addUserType)
        }
    },
    resolve: function resolve(root, params) {
        var user = (0, _users2.default)(params.data);
        var userCreated = user.save();
        if (!userCreated) throw new Error("NO se pudo crear al usuario deseado");
        return userCreated;
    }
};

exports.default = addUser;