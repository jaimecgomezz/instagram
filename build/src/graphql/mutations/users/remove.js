'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _graphql = require('graphql');

var _users = require('../../../models/users');

var _users2 = _interopRequireDefault(_users);

var _users3 = require('../../types/users');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var deleteUser = {
    type: _users3.listUsersType,
    args: {
        id: {
            name: "ID",
            type: new _graphql.GraphQLNonNull(_graphql.GraphQLID)
        }
    },
    resolve: function resolve(root, params) {
        var userDeleted = _users2.default.findByIdAndRemove(params.id);
        if (!userDeleted) throw new Error("No se pudo eliminar al usuario deseado");
        return userDeleted;
    }
};

exports.default = deleteUser;