'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _graphql = require('graphql');

var _users = require('../../../models/users');

var _users2 = _interopRequireDefault(_users);

var _users3 = require('../../types/users');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var updateU = {
    type: _users3.listUsersType,
    args: {
        id: {
            name: "ID",
            type: new _graphql.GraphQLNonNull(_graphql.GraphQLID)
        },
        data: {
            name: "data",
            type: new _graphql.GraphQLNonNull(_users3.addUserType)
        }
    },
    resolve: function resolve(root, params) {
        return _users2.default.findByIdAndUpdate(params.id, {
            $set: _extends({}, params.data)
        }).then(function (userUpdated) {
            return userUpdated;
        }).catch(function (err) {
            console.log("Su error=>", err);
            throw new Error("No se pudo actualizar al usuario deseado");
        });
    }
};

exports.default = updateU;