'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _graphql = require('graphql');

var _photos = require('../../../models/photos');

var _photos2 = _interopRequireDefault(_photos);

var _photos3 = require('../../types/photos');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var addPhoto = {
    type: _photos3.listPhotosType,
    args: {
        data: {
            name: "data",
            type: new _graphql.GraphQLNonNull(_photos3.addPhotoType)
        }
    },
    resolve: function resolve(root, params, context) {
        var photo = (0, _photos2.default)(params.data);
        var photoCreated = photo.save();
        return context.user.then(function (u) {
            return photoCreated.then(function (p) {
                var uP = { userId: u._id, nickname: u.nickname, profilePic: u.profilePic };
                return _photos2.default.findByIdAndUpdate(p._id, { $set: { "userInfo": _extends({}, uP) } }).then(function (pf) {
                    return _photos2.default.findById(pf._id).exec();
                });
            });
        });
    }
};

exports.default = addPhoto;