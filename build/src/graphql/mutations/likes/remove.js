'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _graphql = require('graphql');

var _likes = require('../../../models/likes');

var _likes2 = _interopRequireDefault(_likes);

var _likes3 = require('../../types/likes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var deleteLike = {
    type: _likes3.listLikesType,
    args: {
        id: {
            name: "ID",
            type: new _graphql.GraphQLNonNull(_graphql.GraphQLID)
        }
    },
    resolve: function resolve(root, params) {
        var likeDeleted = _likes2.default.findByIdAndRemove(params.id);
        if (!likeDeleted) throw new Error("No se pudo eliminar el like deseado");
        return likeDeleted;
    }
};

exports.default = deleteLike;