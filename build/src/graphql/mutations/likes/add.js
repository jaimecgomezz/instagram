'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _graphql = require('graphql');

var _photos = require('../../../models/photos');

var _photos2 = _interopRequireDefault(_photos);

var _likes = require('../../../models/likes');

var _likes2 = _interopRequireDefault(_likes);

var _likes3 = require('../../types/likes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var addLike = {
    type: _likes3.listLikesType,
    args: {
        data: {
            name: "data",
            type: new _graphql.GraphQLNonNull(_likes3.addlikesType)
        }
    },
    resolve: function resolve(root, params, context) {
        var like = (0, _likes2.default)(params.data);
        var likeCreated = like.save();
        if (!likeCreated) throw new Error("El like no pudo ser dado");
        return likeCreated.then(function (l) {
            return context.user.then(function (u) {
                var uP = { userId: u._id, nickname: u.nickname, url: u.profilePic };
                return _likes2.default.findByIdAndUpdate(l._id, { $set: { "givenBy": _extends({}, uP) } }).then(function () {
                    return _likes2.default.findById(l._id).exec();
                });
            });
        });
    }
};

exports.default = addLike;