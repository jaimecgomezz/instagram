'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _graphql = require('graphql');

var _photos = require('../../../models/photos');

var _photos2 = _interopRequireDefault(_photos);

var _comments = require('../../../models/comments');

var _comments2 = _interopRequireDefault(_comments);

var _comments3 = require('../../types/comments');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var addComment = {
    type: _comments3.listCommentsType,
    args: {
        data: {
            name: "data",
            type: new _graphql.GraphQLNonNull(_comments3.addCommentType)
        }
    },
    resolve: function resolve(root, params, context) {
        var comment = (0, _comments2.default)(params.data);
        var commentCreated = comment.save();
        if (!commentCreated) throw new Error("NO se pudo crear el commentario deseado");
        return context.user.then(function (u) {
            return commentCreated.then(function (c) {
                var uP = { userId: u._id, nickname: u.nickname, profilePic: u.profilePic };
                _photos2.default.findByIdAndUpdate(c.photoId, { $set: {
                        "lastComment": {
                            "userId": u._id,
                            "content": c.content,
                            "nickname": u.nickname,
                            "profilePic": u.profilePic
                        }
                    } }).exec();
                return _comments2.default.findByIdAndUpdate(c._id, { $set: { "userInfo": _extends({}, uP) } }).then(function (cf) {
                    return _comments2.default.findById(cf._id).exec();
                });
            });
        });
    }
};

exports.default = addComment;