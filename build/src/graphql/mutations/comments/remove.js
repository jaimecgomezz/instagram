'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _graphql = require('graphql');

var _comments = require('../../../models/comments');

var _comments2 = _interopRequireDefault(_comments);

var _comments3 = require('../../types/comments');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var deleteComment = {
    type: _comments3.listCommentsType,
    args: {
        id: {
            name: "ID",
            type: new _graphql.GraphQLNonNull(_graphql.GraphQLID)
        }
    },
    resolve: function resolve(root, params) {
        var commentDeleted = _comments2.default.findByIdAndRemove(params.id);
        if (!commentDeleted) throw new Error("No se pudo eliminar el comentario deseado");
        return commentDeleted;
    }
};

exports.default = deleteComment;