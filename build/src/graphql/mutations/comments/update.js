'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _graphql = require('graphql');

var _comments = require('../../../models/comments');

var _comments2 = _interopRequireDefault(_comments);

var _comments3 = require('../../types/comments');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var updateComments = {
    type: _comments3.listCommentsType,
    args: {
        id: {
            name: "ID",
            type: new _graphql.GraphQLNonNull(_graphql.GraphQLID)
        },
        data: {
            name: "data",
            type: new _graphql.GraphQLNonNull(_comments3.addCommentType)
        }
    },
    resolve: function resolve(root, params) {
        return _comments2.default.findByIdAndUpdate(params.id, {
            $set: _extends({}, params.data)
        }).then(function (commentUpdated) {
            return commentUpdated;
        }).catch(function () {
            throw new Error("No se pudo actualizar el comentario deseado");
        });
    }
};

exports.default = updateComments;