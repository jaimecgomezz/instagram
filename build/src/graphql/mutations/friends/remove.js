'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _graphql = require('graphql');

var _users = require('../../../models/users');

var _users2 = _interopRequireDefault(_users);

var _users3 = require('../../types/users');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    type: _users3.listUsersType,
    args: {
        id: {
            type: _graphql.GraphQLID
        }
    },
    resolve: function resolve(root, params, context) {
        return context.user.then(function (u) {
            return _users2.default.findByIdAndUpdate(u._id, { $pull: { "following": params.id } }).then(function (me) {
                return _users2.default.findByIdAndUpdate(params.id, { $pull: { "followers": u._id } }).then(function (you) {
                    return _users2.default.findById(me._id).exec();
                });
            });
        });
    }
};