'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _graphql = require('graphql');

var _comments = require('../../../models/comments');

var _comments2 = _interopRequireDefault(_comments);

var _comments3 = require('../../types/comments');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var queryAllComments = {
    type: new _graphql.GraphQLList(_comments3.listCommentsType),
    resolve: function resolve() {
        var comments = _comments2.default.find({}).exec();
        if (!comments) throw new Error("No se encontraron los comenatrios de la bd");
        return comments;
    }
};

exports.default = queryAllComments;