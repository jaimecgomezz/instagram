'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _graphql = require('graphql');

var _comments = require('../../../models/comments');

var _comments2 = _interopRequireDefault(_comments);

var _comments3 = require('../../types/comments');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var queryOneComment = {
    type: _comments3.listCommentsType,
    args: {
        id: {
            name: "ID",
            type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLID)
        }
    },
    resolve: function resolve(root, params) {
        var comment = _comments2.default.findById(params.id).exec();
        if (!comment) throw new Error("No se econtro el comentario específico en la bd");
        return comment;
    }
};

exports.default = queryOneComment;