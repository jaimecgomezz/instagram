'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _commentsOnly = require('./commentsOnly');

var _commentsOnly2 = _interopRequireDefault(_commentsOnly);

var _allComments = require('./allComments');

var _allComments2 = _interopRequireDefault(_allComments);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    oneC: _commentsOnly2.default,
    allC: _allComments2.default
};