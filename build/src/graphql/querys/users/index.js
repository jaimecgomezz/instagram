'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _allUsers = require('./allUsers');

var _allUsers2 = _interopRequireDefault(_allUsers);

var _userOnly = require('./userOnly');

var _userOnly2 = _interopRequireDefault(_userOnly);

var _me = require('./me');

var _me2 = _interopRequireDefault(_me);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    allU: _allUsers2.default,
    oneU: _userOnly2.default
    // me
};