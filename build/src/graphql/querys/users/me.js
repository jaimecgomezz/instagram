'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _users = require('../../types/users');

exports.default = {
    type: _users.listUsersType,
    resolve: function resolve(root, params, context) {
        console.log(context.user);
        return context.user;
    }
};