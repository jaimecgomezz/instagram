'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _likesOnly = require('./likesOnly');

var _likesOnly2 = _interopRequireDefault(_likesOnly);

var _allLikes = require('./allLikes');

var _allLikes2 = _interopRequireDefault(_allLikes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    oneL: _likesOnly2.default,
    allL: _allLikes2.default
};