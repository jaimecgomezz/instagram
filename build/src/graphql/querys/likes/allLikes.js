'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _graphql = require('graphql');

var _likes = require('../../../models/likes');

var _likes2 = _interopRequireDefault(_likes);

var _likes3 = require('../../types/likes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var queryAllLikes = {
    type: new _graphql.GraphQLList(_likes3.listLikesType),
    resolve: function resolve() {
        var likes = _likes2.default.find({}).exec();
        if (!likes) throw new Error("No se pudieron consultar los likes de la bd");
        return likes;
    }
};

exports.default = queryAllLikes;