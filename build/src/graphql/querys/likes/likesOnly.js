'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _graphql = require('graphql');

var _likes = require('../../../models/likes');

var _likes2 = _interopRequireDefault(_likes);

var _likes3 = require('../../types/likes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var queryOneLike = {
    type: _likes3.listLikesType,
    args: {
        id: {
            name: "ID",
            type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLID)
        }
    },
    resolve: function resolve(root, params) {
        var like = _likes2.default.find(params.id).exec();
        if (!like) throw new Error("No se enconró el like específico que busca");
        return like;
    }
};

exports.default = queryOneLike;