'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _allPhotos = require('./allPhotos');

var _allPhotos2 = _interopRequireDefault(_allPhotos);

var _photoOnly = require('./photoOnly');

var _photoOnly2 = _interopRequireDefault(_photoOnly);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    allP: _allPhotos2.default,
    oneP: _photoOnly2.default
};