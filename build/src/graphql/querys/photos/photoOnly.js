'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _graphql = require('graphql');

var _photos = require('../../../models/photos');

var _photos2 = _interopRequireDefault(_photos);

var _photos3 = require('../../types/photos');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var queryOnePhoto = {
    type: _photos3.listPhotosType,
    args: {
        id: {
            name: "ID",
            type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLID)
        }
    },
    resolve: function resolve(root, params) {
        var photo = _photos2.default.findById(params.id).exec();
        if (!photo) throw new Error("No se encontó la foto en la base de datos");
        return photo;
    }
};

exports.default = queryOnePhoto;