"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.addlikesType = exports.listLikesType = undefined;

var _graphql = require("graphql");

var listLikesType = exports.listLikesType = new _graphql.GraphQLObjectType({
    name: "listLikes",
    description: "Enlista los likes de una foto",
    fields: function fields() {
        return {
            _id: {
                type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLID)
            },
            photoId: {
                type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLID)
            },
            givenBy: {
                type: new _graphql.GraphQLObjectType({
                    name: "gB",
                    fields: function fields() {
                        return {
                            userId: {
                                type: _graphql.GraphQLID
                            },
                            nickname: {
                                type: _graphql.GraphQLString
                            },
                            url: {
                                type: _graphql.GraphQLString
                            }
                        };
                    }
                })
            },
            givenAt: {
                type: _graphql.GraphQLString
            },
            isActive: {
                type: _graphql.GraphQLBoolean
            }
        };
    }
});

var addlikesType = exports.addlikesType = new _graphql.GraphQLInputObjectType({
    name: "addLikes",
    description: "Modifica los likes de una foto de la bd",
    fields: function fields() {
        return {
            givenBy: {
                type: new _graphql.GraphQLInputObjectType({
                    name: "gBI",
                    fields: function fields() {
                        return {
                            userId: {
                                type: _graphql.GraphQLID
                            },
                            nickname: {
                                type: _graphql.GraphQLString
                            },
                            url: {
                                type: _graphql.GraphQLString
                            }
                        };
                    }
                })
            },
            photoId: {
                type: _graphql.GraphQLString
            },
            isActive: {
                type: _graphql.GraphQLBoolean
            }
        };
    }
});