'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.addUserType = exports.listUsersType = undefined;

var _graphql = require('graphql');

var _photos = require('../../models/photos');

var _photos2 = _interopRequireDefault(_photos);

var _photos3 = require('./photos');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var listUsersType = exports.listUsersType = new _graphql.GraphQLObjectType({
    name: "listUsers",
    fields: function fields() {
        return {
            _id: {
                type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLID)
            },
            nickname: {
                type: _graphql.GraphQLString
            },
            email: {
                type: _graphql.GraphQLString
            },
            isActive: {
                type: _graphql.GraphQLBoolean
            },
            cratedAt: {
                type: _graphql.GraphQLString
            },
            isAdmin: {
                type: _graphql.GraphQLBoolean
            },
            photos: {
                type: (0, _graphql.GraphQLList)((0, _graphql.GraphQLNonNull)(_graphql.GraphQLID))
            },
            followers: {
                type: (0, _graphql.GraphQLList)((0, _graphql.GraphQLNonNull)(_graphql.GraphQLID))
            },
            following: {
                type: (0, _graphql.GraphQLList)((0, _graphql.GraphQLNonNull)(_graphql.GraphQLID))
            },
            profilePic: {
                type: _graphql.GraphQLString
            }
        };
    }
});

var addUserType = exports.addUserType = new _graphql.GraphQLInputObjectType({
    name: "addUsers",
    description: "Puedes modificar usuarios de la base de datos",
    fields: function fields() {
        return {
            nickname: {
                type: _graphql.GraphQLString
            },
            password: {
                type: _graphql.GraphQLString
            },
            email: {
                type: _graphql.GraphQLString
            },
            isActive: {
                type: _graphql.GraphQLBoolean
            },
            isAdmin: {
                type: _graphql.GraphQLBoolean
            },
            userId: {
                type: _graphql.GraphQLString
            },
            profilePic: {
                type: _graphql.GraphQLString
            }
        };
    }
});