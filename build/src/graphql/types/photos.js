'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.addPhotoType = exports.listPhotosType = undefined;

var _graphql = require('graphql');

var _likes = require('../../models/likes');

var _likes2 = _interopRequireDefault(_likes);

var _comments = require('../../models/comments');

var _comments2 = _interopRequireDefault(_comments);

var _likes3 = require('./likes');

var _comments3 = require('./comments');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var listPhotosType = exports.listPhotosType = new _graphql.GraphQLObjectType({
    name: "listPhotos",
    description: "Muestra los datos de fotos de la bd",
    fields: function fields() {
        return {
            _id: {
                type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLID)
            },
            userInfo: {
                type: new _graphql.GraphQLObjectType({
                    name: "userI",
                    fields: function fields() {
                        return {
                            userId: {
                                type: _graphql.GraphQLID
                            },
                            nickname: {
                                type: _graphql.GraphQLString
                            },
                            profilePic: {
                                type: _graphql.GraphQLString
                            }
                        };
                    }
                })
            },
            description: {
                type: _graphql.GraphQLString
            },
            location: {
                type: new _graphql.GraphQLObjectType({
                    name: "location",
                    fields: function fields() {
                        return {
                            city: {
                                type: _graphql.GraphQLString
                            },
                            lat: {
                                type: _graphql.GraphQLString
                            },
                            lon: {
                                type: _graphql.GraphQLString
                            }
                        };
                    }
                })
            },
            cont: {
                type: _graphql.GraphQLInt
            },
            comments: {
                type: new _graphql.GraphQLList(_graphql.GraphQLID),
                resolve: function resolve(photo) {
                    var _id = photo._id;

                    var res = _comments2.default.find({ "photoId": _id }).then(function (com) {
                        return com;
                    });
                    return res;
                }
            },
            likes: {
                type: new _graphql.GraphQLList(_graphql.GraphQLID),
                resolve: function resolve(photo) {
                    var _id = photo._id;

                    var res = _likes2.default.find({ "photoId": _id }).then(function (like) {
                        return like;
                    });
                    return res;
                }
            },
            createdAt: {
                type: _graphql.GraphQLString
            },
            isActive: {
                type: _graphql.GraphQLBoolean
            },
            secure: {
                type: _graphql.GraphQLBoolean
            },
            url: {
                type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
            },
            lastComment: {
                type: new _graphql.GraphQLObjectType({
                    name: "lastC",
                    fields: function fields() {
                        return {
                            userId: {
                                type: _graphql.GraphQLID
                            },
                            nickname: {
                                type: _graphql.GraphQLString
                            },
                            content: {
                                type: _graphql.GraphQLString
                            },
                            profilePic: {
                                type: _graphql.GraphQLString
                            }

                        };
                    }
                })
            }
        };
    }
});

var addPhotoType = exports.addPhotoType = new _graphql.GraphQLInputObjectType({
    name: "addPhotos",
    description: "Modifica las fotos de la base de datos",
    fields: function fields() {
        return {
            userInfo: {
                type: new _graphql.GraphQLInputObjectType({
                    name: "userII",
                    fields: function fields() {
                        return {
                            userId: {
                                type: _graphql.GraphQLID
                            },
                            nickname: {
                                type: _graphql.GraphQLString
                            },
                            profilePic: {
                                type: _graphql.GraphQLString
                            }
                        };
                    }
                })
            },
            description: {
                type: _graphql.GraphQLString
            },
            location: {
                type: new _graphql.GraphQLInputObjectType({
                    name: "modifyLocation",
                    fields: function fields() {
                        return {
                            city: {
                                type: _graphql.GraphQLString
                            },
                            lat: {
                                type: _graphql.GraphQLString
                            },
                            lon: {
                                type: _graphql.GraphQLString
                            }
                        };
                    }
                })
            },
            isActive: {
                type: _graphql.GraphQLBoolean
            },
            secure: {
                type: _graphql.GraphQLBoolean
            },
            url: {
                type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLString)
            },
            lastComment: {
                type: new _graphql.GraphQLInputObjectType({
                    name: "lastCommInput",
                    fields: function fields() {
                        return {
                            userId: {
                                type: _graphql.GraphQLID
                            },
                            nickname: {
                                type: _graphql.GraphQLString
                            },
                            content: {
                                type: _graphql.GraphQLString
                            },
                            profilePic: {
                                type: _graphql.GraphQLString
                            }
                        };
                    }
                })
            }
        };
    }
});