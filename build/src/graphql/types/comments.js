"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.addCommentType = exports.listCommentsType = undefined;

var _graphql = require("graphql");

var listCommentsType = exports.listCommentsType = new _graphql.GraphQLObjectType({
    name: "listComments",
    description: "Enlista los datos de los comentarios de una foto",
    fields: function fields() {
        return {
            _id: {
                type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLID)
            },
            userInfo: {
                type: new _graphql.GraphQLObjectType({
                    name: "userIC",
                    fields: function fields() {
                        return {
                            userId: {
                                type: _graphql.GraphQLID
                            },
                            nickname: {
                                type: _graphql.GraphQLString
                            },
                            profilePic: {
                                type: _graphql.GraphQLString
                            }
                        };
                    }
                })
            },
            photoId: {
                type: (0, _graphql.GraphQLNonNull)(_graphql.GraphQLID)
            },
            content: {
                type: _graphql.GraphQLString
            },
            createdAt: {
                type: _graphql.GraphQLString
            },
            isActive: {
                type: _graphql.GraphQLBoolean
            }
        };
    }
});

var addCommentType = exports.addCommentType = new _graphql.GraphQLInputObjectType({
    name: "addComments",
    description: "Modifica los comentarios de una foto de la bd",
    fields: function fields() {
        return {
            photoId: {
                type: _graphql.GraphQLString
            },
            userInfo: {
                type: new _graphql.GraphQLInputObjectType({
                    name: "userIIC",
                    fields: function fields() {
                        return {
                            userId: {
                                type: _graphql.GraphQLID
                            },
                            nickname: {
                                type: _graphql.GraphQLString
                            },
                            profilePic: {
                                type: _graphql.GraphQLString
                            }
                        };
                    }
                })
            },
            content: {
                type: _graphql.GraphQLString
            },
            isActive: {
                type: _graphql.GraphQLBoolean
            }
        };
    }
});