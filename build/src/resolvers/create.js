'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.createToken = undefined;

var _users = require('../models/users');

var _users2 = _interopRequireDefault(_users);

var _envVar = require('../envVar');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var jwt = require("jsonwebtoken");
var createToken = exports.createToken = function createToken(email, password) {
    if (!email || !password) return null;

    var user = _users2.default.findOne({ "email": email }).then(function (user) {
        var token = new Promise(function (resolve, reject) {
            user.comparePassword(password, function (err, isMatch) {
                if (isMatch) {
                    var payload = {
                        email: user.email,
                        id: user._id,
                        profilePic: user.profilePic
                    };
                    var result = jwt.sign(payload, _envVar.secret, { expiresIn: _envVar.expiresIn });

                    resolve(result);
                } else {
                    console.log("La contraseña no coincide krnal");

                    reject(null);
                }
            });
        });
        return token;
    }).catch(function () {
        reject(null);
    });
    return user;
};