"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _photos = require("../models/photos");

var _photos2 = _interopRequireDefault(_photos);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var likesSchema = new Schema({
    "photoId": {
        type: String
    },
    "givenBy": {
        "userId": {
            type: Schema.Types.ObjectId,
            ref: "Usuarios"
        },
        "nickname": {
            type: String
        },
        "url": {
            type: String
        }
    },
    "givenAt": {
        type: Date,
        default: new Date()
    },
    "isActive": {
        type: Boolean,
        default: true
    }
}, { collection: 'Likes', timestamps: true });

likesSchema.pre('save', function (next) {
    var lik = this;
    _photos2.default.findByIdAndUpdate(lik.photoId, { $push: { "likes": lik._id } }).exec();
    next();
});

exports.default = mongoose.model('Likes', likesSchema);