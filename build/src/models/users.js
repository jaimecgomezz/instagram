"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var mongoose = require("mongoose");
var bcrypt = require("bcryptjs");
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    "nickname": {
        type: String,
        required: true,
        unique: true
    },
    "password": {
        type: String,
        required: true
    },
    "email": {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    "isActive": {
        type: Boolean,
        default: true
    },
    "createdAt": {
        type: Date,
        default: new Date()
    },
    "isAdmin": {
        type: Boolean,
        default: false
    },
    "userId": {
        type: String
    },
    "photos": [{
        type: Schema.Types.ObjectId,
        ref: "Photos"
    }],
    "followers": [{
        type: Schema.Types.ObjectId,
        ref: "Users"
    }],
    "following": [{
        type: Schema.Types.ObjectId,
        ref: "Users"
    }],
    "profilePic": {
        type: String
    }
}, { collection: "Usuarios", timestamps: true });

UserSchema.pre('save', function (next) {
    var user = this;
    if (!user.isModified('password')) {
        console.log("algopasa");return next();
    }
    bcrypt.genSalt(10, function (err, salt) {
        if (err) return next(err);
        bcrypt.hash(user.password, salt, function (err, passHashed) {
            if (err) return next(err);
            user.password = passHashed;
            next();
        });
    });
});

UserSchema.methods.comparePassword = function (tryPass, cb) {
    bcrypt.compare(tryPass, this.password, function (err, same) {
        cb(null, same);
    });
};

exports.default = mongoose.model('Usuarios', UserSchema);