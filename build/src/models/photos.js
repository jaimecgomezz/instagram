"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _users = require("./users");

var _users2 = _interopRequireDefault(_users);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// import {userToken} from './src/envVar'
// import {verifyToken} from '../resolvers/verify'

var photosSchema = new Schema({
    "userInfo": {
        "userId": {
            type: Schema.Types.ObjectId
        },
        "nickname": {
            type: String
        },
        "profilePic": {
            type: String
        }
    },
    "description": {
        type: String,
        required: true
    },
    "location": {
        city: {
            type: String
        },
        lat: {
            type: String
        },
        lon: {
            type: String
        }
    },
    "cont": {
        type: Number
    },
    "comments": [{
        type: Schema.Types.ObjectId,
        ref: "Comments"
    }],
    "likes": [{
        type: Schema.Types.ObjectId,
        ref: "Likes"
    }],
    "createdAt": {
        type: Date,
        default: new Date()
    },
    "isActive": {
        type: Boolean,
        default: true
    },
    "secure": {
        type: Boolean,
        default: false
    },
    "url": {
        type: String
    },
    "lastComment": {
        "userId": {
            type: Schema.Types.ObjectId
        },
        "nickname": {
            type: String
        },
        "content": {
            type: String
        },
        "profilePic": {
            type: String
        }
    }
}, { collection: "Photos", timestamps: true });

photosSchema.pre('save', function (next) {
    var photo = this;
    _users2.default.findByIdAndUpdate(photo.userId, { $push: { "photos": photo._id } }).exec();
    next();
});

exports.default = mongoose.model('Photos', photosSchema);