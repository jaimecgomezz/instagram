"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _photos = require("../models/photos");

var _photos2 = _interopRequireDefault(_photos);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var commentsSchema = new Schema({
    "userInfo": {
        "userId": {
            type: Schema.Types.ObjectId
        },
        "nickname": {
            type: String
        },
        "profilePic": {
            type: String
        }
    },
    "photoId": {
        type: String,
        required: true
    },
    "content": {
        type: String,
        required: true
    },
    "createdAt": {
        type: Date,
        default: new Date()
    },
    "isActive": {
        type: Boolean,
        default: true
    },
    "lastComment": {
        "nickname": {
            type: String
        },
        "content": {
            type: String
        },
        "url": {
            type: String
        }
    }

}, { collection: "Comments", timestamps: true });

commentsSchema.pre('save', function (next) {
    var com = this;
    _photos2.default.findByIdAndUpdate(com.photoId, { $push: { "comments": com._id } }).exec();
    next();
});

exports.default = mongoose.model('Comments', commentsSchema);