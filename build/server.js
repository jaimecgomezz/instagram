"use strict";

var _users = require("./src/models/users");

var _users2 = _interopRequireDefault(_users);

var _graphql = require("./src/graphql");

var _graphql2 = _interopRequireDefault(_graphql);

var _envVar = require("./src/envVar");

var _create = require("./src/resolvers/create");

var _verify = require("./src/resolvers/verify");

var _users3 = require("./src/graphql/querys/users");

var _graphql3 = require("graphql");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var graphQLHTTP = require("express-graphql");
var express = require("express");
var mongoose = require("mongoose");
var app = express();
var bodyParser = require("body-parser");

var PORT = process.env.PORT || 3000;

mongoose.connect("mongodb://JaimeCGomez:1a2b3c@ds125831.mlab.com:25831/instagram-cn", { useNewUrlParser: true });

// mongoose.connect("mongodb://localhost:27017/local", {useNewUrlParser:true});

var db = mongoose.connection;
db.on('error', function () {
    console.log("No fue posible conectar con la base de datos madafaka");
}).once('open', function () {
    console.log("Se pudo conectar con la base de datos chavo");
});

app.use(bodyParser.json());

// app.use('/graphql', (req, res, next)=>{
//     const token = req.headers['authorization'];
//     try{
//         req.user = verifyToken(token);
//         next();
//     }catch(error){
//         res.status(401).json({message: error.message})
//     }
// })

app.use('/graphql', graphQLHTTP(function (req, res) {
    return {
        schema: _graphql2.default,
        graphiql: true,
        pretty: true,
        context: {
            user: req.user
        }
    };
}));

app.get('/', function (req, res) {
    // res.send({"message":"holi"})
    res.send("Estoy jalandoooo");
});

app.post('/signUp', function (req, res) {
    var user = req.body;
    _users2.default.create(user).then(function (user) {
        res.status(201).json({
            "Fel": "Felicidades, ha logrado crear su cuenta",
            "id": user._id
        });
    }).catch(function (error) {
        res.status(400).json({ message: error.message });
    });
});

app.post('/login', function (req, res, next) {
    var user = req.body;
    (0, _create.createToken)(user.email, user.password).then(function (token) {
        res.status(201).json({ token: token });
    }).catch(function (err) {
        res.status(403).json({ "Mensaje": "Hubo un problema al verificar credenciales" });
    });
});

app.post('/query', function (req, res, next) {
    var lol = req.body;
    (0, _graphql3.graphql)(_users3.queryUsersType, query).then(function (resu) {
        console.log(resu);
        res.send(resu);
    });
});

app.listen(PORT, function () {
    console.log("Estoy funcionando en el puerto 3000");
});