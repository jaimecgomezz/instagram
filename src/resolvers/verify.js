import Users from '../models/users';
import jwt from 'jsonwebtoken';
import {secret, tokenPrefix} from '../envVar';

export const verifyToken = (token) =>{
    try{
        const [prefix, recivedToken] = token.split(' ');
        let user = null;
        if(!recivedToken){
            throw new Error("No token provided");
        }
        if(prefix != tokenPrefix){
            throw new Error("Invalid header format");
        }
        jwt.verify(recivedToken, secret, (err, payload)=>{
            if(err){
                throw new Error("Invalid token");
            }else{
                user = Users.findById(payload.id).exec()
            }
        })
        if(!user){
            throw new Error("User don´t exist in db");
        }
        return user;
    }catch(error){
        throw new Error("Error not expected")
    }
}