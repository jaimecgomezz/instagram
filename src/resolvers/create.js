const jwt = require("jsonwebtoken");
import Users from '../models/users';
import {expiresIn, secret} from '../envVar'

export const createToken = function(email, password){
    if(!email || !password) return null;

    let user = Users.findOne({"email":email}).then((user)=>{
        let token = new Promise((resolve, reject)=>{
            user.comparePassword(password, (err, isMatch)=>{
                if(isMatch){
                    let payload = {
                        email: user.email,
                        id:user._id,
                        profilePic: user.profilePic
                    };
                    let result = jwt.sign(payload,secret, {expiresIn});

                    resolve (result);
                }else{
                    console.log("La contraseña no coincide krnal");
                    
                    reject(null);
                }
            });
        });
        return token;
    }).catch(function(){
        reject(null);
    });
    return user;
}