const mongoose = require("mongoose");
const Schema = mongoose.Schema;

import Photos from '../models/photos'

const likesSchema = new Schema({
    "photoId":{
        type: String,
    },
    "givenBy":{
        "userId":{
            type: Schema.Types.ObjectId,
            ref: "Usuarios"
        },
        "nickname":{
            type: String
        },
        "url":{
            type: String,
        }
    },
    "givenAt":{
        type:Date,
        default:new Date,
    },
    "isActive":{
        type:Boolean,
        default:true,
    }
}, {collection:'Likes', timestamps:true});

likesSchema.pre('save', function(next){
    let lik = this;
    Photos.findByIdAndUpdate(lik.photoId, {$push:{"likes":lik._id}}).exec();
    next();
})

export default mongoose.model('Likes', likesSchema);