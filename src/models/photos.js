const mongoose = require("mongoose");
const Schema = mongoose.Schema;

import Users from './users'
// import {userToken} from './src/envVar'
// import {verifyToken} from '../resolvers/verify'

const photosSchema = new Schema({
    "userInfo":{
        "userId":{
            type: Schema.Types.ObjectId
        },
        "nickname":{
            type: String,
        },
        "profilePic":{
            type: String
        }
    },
    "description":{
        type:String,
        required:true,
    },
    "location":{
        city: {
            type: String
        },
        lat: {
            type: String
        },
        lon: {
            type: String
        }
    },
    "cont":{
        type: Number,
    },
    "comments": [{
        type: Schema.Types.ObjectId,
        ref: "Comments"
    }],
    "likes":[{
        type: Schema.Types.ObjectId,
        ref: "Likes"
    }],
    "createdAt":{
        type:Date,
        default: new Date()
    },
    "isActive":{
        type:Boolean,
        default:true
    },
    "secure":{
        type:Boolean,
        default:false
    },
    "url":{
        type:String
    },
    "lastComment":{
        "userId":{
            type: Schema.Types.ObjectId
        },
        "nickname":{
            type: String
        },
        "content":{
            type: String
        },
        "profilePic":{
            type: String,
        }
    }
}, {collection:"Photos", timestamps:true});

photosSchema.pre('save', function(next){
    let photo = this;
    Users.findByIdAndUpdate(photo.userId, {$push:{"photos":photo._id}}).exec();
    next();
})

export default mongoose.model('Photos', photosSchema);
