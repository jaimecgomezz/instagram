const mongoose = require("mongoose");
const Schema = mongoose.Schema;

import Photos from '../models/photos'

const commentsSchema = new Schema({
    "userInfo":{
        "userId":{
            type: Schema.Types.ObjectId,
        },
        "nickname":{
            type: String,
        },
        "profilePic":{
            type: String
        },
    },
    "photoId":{
        type: String,
        required:true,
    },
    "content":{
        type: String,
        required:true,
    },
    "createdAt":{
        type:Date,
        default: new Date()
    },
    "isActive":{
        type: Boolean,
        default: true,
    },
    "lastComment":{
        "nickname":{
            type: String
        },
        "content":{
            type: String,
        },
        "url":{
            type: String
        }
    }

}, {collection: "Comments", timestamps:true});

commentsSchema.pre('save', function(next){
    let com = this;
    Photos.findByIdAndUpdate(com.photoId, {$push:{"comments":com._id}}).exec();
    next();
})

export default mongoose.model('Comments', commentsSchema);