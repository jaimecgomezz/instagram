const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    "nickname": {
        type:String,
        required:true,
        unique:true
    },
    "password":{
        type:String,
        required:true,
    },
    "email":{
        type:String,
        required:true,
        index:true,
        unique:true,
    },
    "isActive":{
        type:Boolean,
        default:true
    },
    "createdAt":{
        type:Date,
        default: new Date()
    },
    "isAdmin":{
        type:Boolean,
        default:false
    },
    "userId":{
        type:String
    }, 
    "photos":[{
        type: Schema.Types.ObjectId,
        ref: "Photos"
    }],
    "followers":[{
        type: Schema.Types.ObjectId,
        ref: "Users"
    }],
    "following":[{
        type: Schema.Types.ObjectId,
        ref: "Users"
    }],
    "profilePic":{
        type: String
    }
}, {collection:"Usuarios", timestamps:true});

UserSchema.pre('save', function(next){
    let user=this;
    if(!user.isModified('password')){ console.log("algopasa");return next(); }
    bcrypt.genSalt(10, function(err, salt){
        if(err) return next(err);
        bcrypt.hash(user.password, salt, function(err, passHashed){
            if(err) return next(err);
            user.password = passHashed;
            next();
        });
    })
})

UserSchema.methods.comparePassword= function(tryPass,cb){
    bcrypt.compare(tryPass, this.password, (err, same)=>{
        cb(null, same);
    })
}

export default mongoose.model('Usuarios', UserSchema);