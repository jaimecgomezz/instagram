import {listUsersType} from '../../types/users';

export default{
    type: listUsersType,
    resolve(root,params, context){
        console.log(context.user)
        return context.user;
    }
};