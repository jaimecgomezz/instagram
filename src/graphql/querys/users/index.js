import allU from './allUsers'
import oneU from './userOnly'
import me from './me';

export default {
    allU,
    oneU,
    // me
};