import {
    GraphQLNonNull,
    GraphQLID
} from 'graphql';

import Users from '../../../models/users';
import {listUsersType} from '../../types/users';

const queryOneUser = {
    type: listUsersType,
    arg: {
        id:{
          name: "ID",
          type: GraphQLNonNull(GraphQLID),  
        }
    },
    resolve(root, params){
        const user = Users.findById(params.id).exec();
        if(!user) throw new Error("No se halló  al usuarion de la base de datos");
        return user;
    }
};

export default queryOneUser;