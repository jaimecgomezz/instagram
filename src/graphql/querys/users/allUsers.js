import {
    GraphQLList
} from 'graphql';

import Users from '../../../models/users';
import {listUsersType} from '../../types/users';

const queryAllUsers = {
    type: new GraphQLList(listUsersType),
    resolve(){
        const allUsers = Users.find({}).exec();
        if(!allUsers) throw new Error("Error al traer elementos de la base de datos");
        return allUsers;
    }
};

export default queryAllUsers;