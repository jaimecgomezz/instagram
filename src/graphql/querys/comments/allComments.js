import {
    GraphQLList
} from 'graphql';

import Comments from '../../../models/comments';
import {listCommentsType} from '../../types/comments';

const queryAllComments = {
    type: new GraphQLList(listCommentsType),
    resolve(){
        const comments = Comments.find({}).exec();
        if(!comments) throw new Error("No se encontraron los comenatrios de la bd");
        return comments;
    }
};

export default queryAllComments;