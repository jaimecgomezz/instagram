import {
    GraphQLNonNull,
    GraphQLID
} from 'graphql';

import Comments from '../../../models/comments';
import {listCommentsType} from '../../types/comments';

const queryOneComment = {
    type: listCommentsType,
    args: {
        id:{
            name: "ID",
            type: GraphQLNonNull(GraphQLID),
        }
    },
    resolve(root, params){
        const comment = Comments.findById(params.id).exec();
        if(!comment) throw new Error("No se econtro el comentario específico en la bd");
        return comment;
    }
};

export default queryOneComment;