import {
    GraphQLList,
} from 'graphql'

import Likes from '../../../models/likes';
import {listLikesType} from '../../types/likes';

const queryAllLikes = {
    type: new GraphQLList(listLikesType),
    resolve(){
        const likes = Likes.find({}).exec();
        if(!likes) throw new Error("No se pudieron consultar los likes de la bd");
        return likes;
    }
};

export default queryAllLikes;