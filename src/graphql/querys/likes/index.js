import oneL from './likesOnly';
import allL from './allLikes';

export default {
    oneL,
    allL,
}