import {
    GraphQLNonNull,
    GraphQLID,
} from 'graphql'

import Likes from '../../../models/likes';
import {listLikesType} from '../../types/likes';

const queryOneLike = {
    type: listLikesType,
    args: {
        id:{
            name: "ID",
            type: GraphQLNonNull(GraphQLID),
        },
    },
    resolve(root, params){
        const like = Likes.find(params.id).exec();
        if(!like) throw new Error("No se enconró el like específico que busca");
        return like;
    }
};

export default queryOneLike;