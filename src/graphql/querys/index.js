import comments from './comments';
import likes from './likes';
import photos from './photos';
import users from './users';

export default {
    ...comments,
    ...likes,
    ...photos,
    ...users,
}