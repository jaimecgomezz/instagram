import allP from './allPhotos';
import oneP from './photoOnly';

export default {
    allP,
    oneP,
}