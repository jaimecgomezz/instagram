import {
    GraphQLNonNull,
    GraphQLID,
} from 'graphql'

import Photos from '../../../models/photos';
import {listPhotosType} from '../../types/photos'

const queryOnePhoto = {
    type: listPhotosType,
    args: {
        id: {
            name: "ID",
            type: GraphQLNonNull(GraphQLID),
        }
    },
    resolve(root, params){
        const photo = Photos.findById(params.id).exec();
        if(!photo) throw new Error("No se encontó la foto en la base de datos");
        return photo;
    }
}

export default queryOnePhoto;