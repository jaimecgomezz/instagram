import {
    GraphQLList,
} from 'graphql'
 import Photos from '../../../models/photos';
 import {listPhotosType} from '../../types/photos'

 const queryAllPhotos = {
     type: new GraphQLList(listPhotosType),
     resolve(){
         const photos = Photos.find({}).exec();
         if(!photos) throw new Error("No se pudieron extraer las fotos de la bd");
         return photos;
     }
 };

 export default queryAllPhotos;