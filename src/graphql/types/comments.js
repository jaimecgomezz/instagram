import {
    GraphQLInputObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLObjectType,
    GraphQLBoolean,
    GraphQLNonNull,
} from 'graphql';

export const listCommentsType = new GraphQLObjectType({
    name: "listComments",
    description: "Enlista los datos de los comentarios de una foto",
    fields:()=>({
        _id:{
            type: GraphQLNonNull(GraphQLID),
        },
        userInfo:{
            type: new GraphQLObjectType({
                name: "userIC",
                fields: ()=>({
                    userId:{
                        type: GraphQLID
                    },
                    nickname:{
                        type: GraphQLString,
                    },
                    profilePic:{
                        type: GraphQLString
                    },
                })
            }),            
        },
        photoId:{
            type: GraphQLNonNull(GraphQLID),
        },
        content:{
            type: GraphQLString,
        },
        createdAt:{
            type: GraphQLString,
        },
        isActive:{
            type: GraphQLBoolean,
        },
    })
});

export const addCommentType = new GraphQLInputObjectType({
    name: "addComments",
    description: "Modifica los comentarios de una foto de la bd",
    fields: ()=>({
        photoId:{
            type: GraphQLString
        },
        userInfo:{
            type: new GraphQLInputObjectType({
                name: "userIIC",
                fields: ()=>({
                    userId:{
                        type: GraphQLID
                    },
                    nickname:{
                        type: GraphQLString,
                    },
                    profilePic:{
                        type: GraphQLString
                    },
                })
            }),
        },
        content:{
            type: GraphQLString,
        },
        isActive:{
            type: GraphQLBoolean,
        },
    })
});