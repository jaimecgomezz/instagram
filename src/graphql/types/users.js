import {
    GraphQLInputObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLObjectType,
    GraphQLBoolean,
    GraphQLNonNull,
    GraphQLSchema,
    GraphQLList,
} from 'graphql';

import Photos from '../../models/photos';
import {listPhotosType} from './photos'

export const listUsersType = new GraphQLObjectType({
    name: "listUsers",
    fields: ()=>({
        _id:{
            type: GraphQLNonNull(GraphQLID),
        },
        nickname:{
            type: GraphQLString
        },
        email:{
            type:GraphQLString,
        },
        isActive:{
            type:GraphQLBoolean,
        },
        cratedAt:{
            type:GraphQLString
        },
        isAdmin:{
            type: GraphQLBoolean,
        },
        photos:{
            type: GraphQLList(GraphQLNonNull(GraphQLID)),
        }, 
        followers:{
            type: GraphQLList(GraphQLNonNull(GraphQLID))
        },
        following:{
            type: GraphQLList(GraphQLNonNull(GraphQLID))
        },
        profilePic:{
            type: GraphQLString,
        }
    })
});

export const addUserType = new GraphQLInputObjectType({
    name: "addUsers",
    description: "Puedes modificar usuarios de la base de datos",
    fields:()=>({
        nickname:{
            type: GraphQLString,
        },
        password:{
            type: GraphQLString,
        },
        email:{
            type: GraphQLString,
        },
        isActive:{
            type: GraphQLBoolean,
        },
        isAdmin:{
            type: GraphQLBoolean,
        },
        userId:{
            type: GraphQLString,
        },
        profilePic:{
            type: GraphQLString
        }
    })
}) 
