import {
    GraphQLInputObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLObjectType,
    GraphQLBoolean,
    GraphQLNonNull,
} from 'graphql';

export const listLikesType = new GraphQLObjectType({
    name: "listLikes",
    description: "Enlista los likes de una foto",
    fields: ()=>({
        _id:{
            type: GraphQLNonNull(GraphQLID),
        },
        photoId:{
            type: GraphQLNonNull(GraphQLID),
        },
        givenBy:{
            type: new GraphQLObjectType({
                name: "gB",
                fields: ()=>({
                    userId:{
                        type: GraphQLID,
                    },
                    nickname:{
                        type: GraphQLString,
                    },
                    url:{
                        type: GraphQLString,
                    }
                })
            })
        },
        givenAt:{
            type: GraphQLString,
        },
        isActive:{
            type: GraphQLBoolean,
        },
    })
});

export const addlikesType = new GraphQLInputObjectType({
    name: "addLikes",
    description: "Modifica los likes de una foto de la bd",
    fields: ()=>({
        givenBy:{
            type: new GraphQLInputObjectType({
                name: "gBI",
                fields: ()=>({
                    userId:{
                        type: GraphQLID
                    },
                    nickname:{
                        type: GraphQLString,
                    },
                    url:{
                        type: GraphQLString,
                    }
                })
            })
        },
        photoId:{
            type:GraphQLString,
        },
        isActive:{
            type: GraphQLBoolean,
        },
    })
});