import {
    GraphQLInputObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLObjectType,
    GraphQLBoolean,
    GraphQLNonNull,
    GraphQLList,
    GraphQLInt,
} from 'graphql';

import Likes from '../../models/likes';
import Comments from '../../models/comments';
import {listLikesType} from './likes';
import {listCommentsType} from './comments';

export const listPhotosType = new GraphQLObjectType({
    name: "listPhotos",
    description: "Muestra los datos de fotos de la bd",
    fields: ()=>({
        _id:{
            type: GraphQLNonNull(GraphQLID),
        },
        userInfo:{
            type: new GraphQLObjectType({
                name: "userI",
                fields: ()=>({
                    userId:{
                        type: GraphQLID,
                    },
                    nickname:{
                        type: GraphQLString,
                    },
                    profilePic:{
                        type: GraphQLString
                    }
                })
            }),
        },
        description: {
            type: GraphQLString,
        },
        location:{
            type: new GraphQLObjectType({
                name: "location",
                fields: ()=>({
                    city:{
                        type: GraphQLString,
                    },
                    lat:{
                        type: GraphQLString,
                    },
                    lon:{
                        type: GraphQLString,
                    }
                })
            }),
        },
        cont:{
            type: GraphQLInt,
        },
        comments:{
            type: new GraphQLList(GraphQLID),
            resolve(photo){
                const {_id} = photo;
                const res = Comments.find({"photoId":_id}).then((com)=>{
                    return com;
                });
                return res;
            }
        },
        likes:{
            type: new GraphQLList(GraphQLID),
            resolve(photo){
                const {_id} = photo;
                const res = Likes.find({"photoId":_id}).then((like)=>{
                    return like;
                });
                return res;
            }
        },
        createdAt:{
            type: GraphQLString,
        },
        isActive:{
            type: GraphQLBoolean,
        },
        secure:{
            type: GraphQLBoolean,
        },
        url:{
            type: GraphQLNonNull(GraphQLString),
        },
        lastComment:{
            type: new GraphQLObjectType({
                name: "lastC",
                fields: ()=>({
                    userId:{
                        type: GraphQLID
                    },
                    nickname:{
                        type: GraphQLString
                    },
                    content:{
                        type: GraphQLString,
                    },
                    profilePic:{
                        type: GraphQLString
                    }

                })
            })
        }
    })
});

export const addPhotoType = new GraphQLInputObjectType({
    name: "addPhotos",
    description: "Modifica las fotos de la base de datos",
    fields: ()=>({
        userInfo:{
            type: new GraphQLInputObjectType({
                name : "userII",
                fields:()=>({
                    userId:{
                        type: GraphQLID
                    },
                    nickname:{
                        type: GraphQLString,
                    },
                    profilePic:{
                        type: GraphQLString,
                    },
                }),
            }),
        },
        description:{
            type: GraphQLString,
        },
        location:{
            type: new GraphQLInputObjectType({
                name: "modifyLocation",
                fields: ()=>({
                    city:{
                        type: GraphQLString,
                    },
                    lat:{
                        type: GraphQLString,
                    },
                    lon:{
                        type: GraphQLString,    
                    }
                })
            }),
        },
        isActive:{
            type: GraphQLBoolean,
        },
        secure:{
            type: GraphQLBoolean,
        },
        url:{
            type: GraphQLNonNull(GraphQLString),
        },
        lastComment:{
            type: new GraphQLInputObjectType({
                name: "lastCommInput",
                fields: ()=>({
                    userId:{
                        type: GraphQLID
                    },
                    nickname:{
                        type: GraphQLString,
                    },
                    content:{
                        type: GraphQLString
                    },
                    profilePic:{
                        type: GraphQLString
                    }
                })
            })
        }
    }),
})
