import {
    GraphQLID,
} from 'graphql'

import Users from '../../../models/users';
import {listUsersType, addUserType} from '../../types/users'

export default  {
    type:listUsersType,
    args:{
        id:{
            name: "ID",
            type: GraphQLID
        }
    },
    resolve(root, params, context){
        return context.user.then((u)=>{
            return Users.findByIdAndUpdate(u._id, {$push:{"following":params.id}}).then((me)=>{
                return Users.findByIdAndUpdate(params.id, {$push:{"followers":me._id}}).then((you)=>{
                    return Users.findById(me._id).exec();
                })
            })
        })
        
    }
}