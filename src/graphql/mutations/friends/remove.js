import {
    GraphQLID,
} from 'graphql'

import Users from '../../../models/users';
import {listUsersType, addUserType} from '../../types/users'

export default {
    type: listUsersType,
    args:{
        id:{
            type: GraphQLID,
        }
    },
    resolve(root, params, context){
        return context.user.then((u)=>{
            return Users.findByIdAndUpdate(u._id, {$pull:{"following":params.id}}).then((me)=>{
                return Users.findByIdAndUpdate(params.id, {$pull:{"followers":u._id}}).then((you)=>{
                    return Users.findById(me._id).exec();
                })
            })
        })
    }
}