import addC from './add';
import removeC from './remove';
import updateC from './update';

export default {
    addC,
    removeC,
    updateC
}