import {
    GraphQLList,
    GraphQLNonNull,
    GraphQLID
} from 'graphql';

import Comments from '../../../models/comments';
import {listCommentsType, addCommentType } from '../../types/comments';

const updateComments = {
    type: listCommentsType,
    args:{
        id:{
            name: "ID",
            type: new GraphQLNonNull(GraphQLID),
        },
        data:{
            name:"data",
            type: new GraphQLNonNull(addCommentType), 
        },
    },
        resolve(root, params){
            return Comments.findByIdAndUpdate(params.id, {
                $set:{...params.data}
            }).then((commentUpdated)=>{
                return commentUpdated;
            }).catch(()=>{
                throw new Error("No se pudo actualizar el comentario deseado");
            });
        }
};

export default updateComments;