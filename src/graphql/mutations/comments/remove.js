import {
    GraphQLNonNull,
    GraphQLID
} from 'graphql'

import Comments from '../../../models/comments';
import {listCommentsType, addCommentType } from '../../types/comments';

const deleteComment = {
    type: listCommentsType,
    args:{
        id:{
            name: "ID",
            type: new GraphQLNonNull(GraphQLID)
        },
    },
    resolve(root, params){
        const commentDeleted = Comments.findByIdAndRemove(params.id);
        if(!commentDeleted) throw new Error("No se pudo eliminar el comentario deseado");
        return commentDeleted;
    }
};

export default deleteComment;