import {
    GraphQLNonNull,
} from 'graphql'

import Photos from '../../../models/photos'
import Comments from '../../../models/comments';
import {listCommentsType, addCommentType } from '../../types/comments';

const addComment = {
    type: listCommentsType,
    args:{
        data:{
            name: "data",
            type: new GraphQLNonNull(addCommentType)
        }
    },
    resolve(root, params, context){
        const comment = Comments(params.data);
        const commentCreated = comment.save();
        if(!commentCreated) throw new Error("NO se pudo crear el commentario deseado");
        return context.user.then((u)=>{
            return commentCreated.then((c)=>{
                let uP = {userId:u._id, nickname:u.nickname, profilePic:u.profilePic};
                Photos.findByIdAndUpdate(c.photoId, {$set:{
                    "lastComment":{
                        "userId":u._id,
                        "content":c.content,
                        "nickname": u.nickname,
                        "profilePic": u.profilePic
                    }
                }}).exec()
                return Comments.findByIdAndUpdate(c._id, {$set:{"userInfo":{...uP}}}).then((cf)=>{
                    return Comments.findById(cf._id).exec();
                })
            })
        })
    }
};

export default addComment;