import {
    GraphQLNonNull,
} from 'graphql'

import Photos from '../../../models/photos';
import Likes from '../../../models/likes';
import {listLikesType, addlikesType} from '../../types/likes';

const addLike = {
    type: listLikesType,
    args:{
        data:{
            name: "data",
            type: new GraphQLNonNull(addlikesType)
        }
    },
    resolve(root, params, context){
        const like = Likes(params.data);
        const likeCreated = like.save();
        if(!likeCreated) throw new Error("El like no pudo ser dado");
        return likeCreated.then((l)=>{
            return context.user.then((u)=>{
                let uP = {userId:u._id, nickname:u.nickname, url:u.profilePic};
                return Likes.findByIdAndUpdate(l._id, {$set:{"givenBy":{...uP}}}).then(()=>{
                    return Likes.findById(l._id).exec();
                })
            })
        });
    }
};

export default addLike;