import addL from './add';
import removeL from './remove';
import updateL from './update';

export default {
    addL,
    removeL,
    updateL
}