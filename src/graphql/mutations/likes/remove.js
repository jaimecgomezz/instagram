import {
    GraphQLNonNull,
    GraphQLID
} from 'graphql'

import Likes from '../../../models/likes';
import {listLikesType} from '../../types/likes';

const deleteLike = {
    type: listLikesType,
    args:{
        id:{
            name: "ID",
            type: new GraphQLNonNull(GraphQLID)
        },
    },
    resolve(root, params){
        const likeDeleted = Likes.findByIdAndRemove(params.id);
        if(!likeDeleted) throw new Error("No se pudo eliminar el like deseado");
        return likeDeleted;
    }
};

export default deleteLike;