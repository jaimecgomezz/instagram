import {
    GraphQLList,
    GraphQLNonNull,
    GraphQLID
} from 'graphql';

import Likes from '../../../models/likes';
import {listLikesType, addlikesType} from '../../types/likes';

const updateLike = {
    type: listLikesType,
    args:{
        id:{
            name: "ID",
            type: new GraphQLNonNull(GraphQLID),
        },
        data:{
            name: "data",
            type: new GraphQLNonNull(addlikesType), 
        },
    },
        resolve(root, params){
            return Likes.findByIdAndUpdate(params.id, {
                $set:{...params.data}
            }).then((likeUpdated)=>{
                return likeUpdated;
            }).catch(()=>{
                throw new Error("No se pudo actualizar el like deseado");
            });
        }
};

export default updateLike;