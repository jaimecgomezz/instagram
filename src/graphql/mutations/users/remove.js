import {
    GraphQLList,
    GraphQLNonNull,
    GraphQLID
} from 'graphql'

import Users from '../../../models/users';
import {listUsersType, addUserType} from '../../types/users';

const deleteUser = {
    type: listUsersType,
    args:{
        id:{
            name: "ID",
            type: new GraphQLNonNull(GraphQLID)
        },
    },
    resolve(root, params){
        const userDeleted = Users.findByIdAndRemove(params.id);
        if(!userDeleted) throw new Error("No se pudo eliminar al usuario deseado");
        return userDeleted;
    }
};

export default deleteUser;