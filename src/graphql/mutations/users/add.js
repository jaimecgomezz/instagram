import {
    GraphQLNonNull,
} from 'graphql'

import Users from '../../../models/users';
import {listUsersType, addUserType} from '../../types/users';

const addUser = {
    type: listUsersType,
    args:{
        data:{
            name: "data",
            type: new GraphQLNonNull(addUserType)
        }
    },
    resolve(root, params){
        const user = Users(params.data);
        const userCreated = user.save();
        if(!userCreated) throw new Error("NO se pudo crear al usuario deseado");
        return userCreated;
    }
};

export default addUser;