import addU from './add';
import removeU from './remove';
import updateU from './update';

export default {
    addU,
    removeU,
    updateU
}