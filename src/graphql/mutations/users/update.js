import {
    GraphQLList,
    GraphQLNonNull,
    GraphQLID
} from 'graphql'

import Users from '../../../models/users';
import {listUsersType, addUserType} from '../../types/users';

const updateU =  {
    type: listUsersType,
    args:{
        id:{
            name: "ID",
            type: new GraphQLNonNull(GraphQLID),
        },
        data:{
            name: "data",
            type: new GraphQLNonNull(addUserType), 
        },
    },
        resolve(root, params){
            return Users.findByIdAndUpdate(params.id,{
                $set:{...params.data}
            }).then((userUpdated)=>{
                return userUpdated;
            }).catch((err)=>{
                console.log("Su error=>", err)
                throw new Error("No se pudo actualizar al usuario deseado");
            });
        }
};

export default updateU;
