import {
    GraphQLList,
    GraphQLNonNull,
    GraphQLID
} from 'graphql';

import Photos from '../../../models/photos';
import {listPhotosType, addPhotoType } from '../../types/photos';

const updatePhoto = {
    type: listPhotosType,
    args:{
        id:{
            name: "ID",
            type: new GraphQLNonNull(GraphQLID),
        },
        data:{
            name: "data",
            type: new GraphQLNonNull(addPhotoType), 
        },
    },
        resolve(root, params){
            return Photos.findByIdAndUpdate(params.id, {
                $set:{...params.data}
            }).then((photoUpdated)=>{
                return photoUpdated;
            }).catch(()=>{
                throw new Error("No se pudo actualizar la foto deseada");
            });
        }
};

export default updatePhoto;