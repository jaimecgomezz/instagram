import {
    GraphQLNonNull,
} from 'graphql'

import Photos from '../../../models/photos';
import {listPhotosType, addPhotoType } from '../../types/photos';

const addPhoto = {
    type: listPhotosType,
    args:{
        data:{
            name: "data",
            type: new GraphQLNonNull(addPhotoType)
        }
    },
    resolve(root, params, context){
        const photo = Photos(params.data);
        const photoCreated = photo.save(); 
        return context.user.then((u)=>{
            return photoCreated.then((p)=>{
                let uP = {userId:u._id, nickname:u.nickname, profilePic:u.profilePic};
                return Photos.findByIdAndUpdate(p._id, {$set:{"userInfo":{...uP}}}).then((pf)=>{
                    return Photos.findById(pf._id).exec();
                });
            })
        })
    }
};

export default addPhoto;