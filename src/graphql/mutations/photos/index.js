import addP from './add';
import removeP from './remove';
import updateP from './update';

export default {
    addP,
    removeP,
    updateP
}