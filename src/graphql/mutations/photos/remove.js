import {
    GraphQLNonNull,
    GraphQLID
} from 'graphql'

import Photos from '../../../models/photos';
import {listPhotosType} from '../../types/photos';

const deletePhoto = {
    type: listPhotosType,
    args:{
        id:{
            name: "ID",
            type: new GraphQLNonNull(GraphQLID)
        },
    },
    resolve(root, params){
        const photoDeleted = Photos.findByIdAndRemove(params.id);
        if(!photoDeleted) throw new Error("No se pudo eliminar la foto deseada");
        return photoDeleted;
    }
};

export default deletePhoto;