import Users from './users';
import Comments from './comments';
import Likes from './likes';
import Photos from './photos';
import Friends from './friends'

export default{
    ...Users,
    ...Comments,
    ...Likes,
    ...Photos,
    ...Friends
}